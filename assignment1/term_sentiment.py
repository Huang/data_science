# -*- coding: utf-8 -*-

import sys
import json
import time

def load_sent(afinnfile):
    # initialize an empty dictionary
    sentiment_dict = {}

    """
    {
        'have':
            {
                'max_len':4,
                '2':{
                      'have fun':4,
                      ...
                    },
                '4':{
                       'have a nice day':5,
                       '...':..,
                    }
            }
        '': ...
    }
    """

    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"

        first_word = term.split(" ")[0]
        term_len = len(term.split(" "))
        if first_word not in sentiment_dict:
            sentiment_dict[first_word] = {}
            sentiment_dict[first_word]["max_len"] = 0
        if term_len not in sentiment_dict[first_word]:
            sentiment_dict[first_word][term_len] = {}
        sentiment_dict[first_word][term_len][term] = int(score)

        # UPDATE MAX_LEN
        if term_len > sentiment_dict[first_word]["max_len"]:
            sentiment_dict[first_word]["max_len"] = term_len

    return sentiment_dict


def get_sentence_sentiment_list(sentence, sentiment_dict):

    #print len(sentiment_dict)
    sentence_list = sentence.strip().split()
    match_list = []
    mismatch_list = []
    for idx, word in enumerate(sentence_list):
        #print word
        if word not in sentiment_dict:
            #print "<0>"
            mismatch_list.append(word)
            continue
        for term_len in range(sentiment_dict[word]['max_len'], 0, -1):
            if term_len not in sentiment_dict[word]:
                #print "<1>"
                continue
            if ( idx + term_len - 1 ) > len(sentence_list):
                #print "<2>"
                continue

            candidate_word = " ".join(sentence_list[idx: (idx + term_len)])
            #print "c> ", sentence_list[idx: (idx + term_len)]
            #print candidate_word
            #print sentence_list, idx, (idx + term_len - 1)
            for term, score in sentiment_dict[word][term_len].items():
                if candidate_word == term:
                    #print "HIT !"
                    match_list.append({term:score})
                    #match_list.extend(get_sentence_sentiment_list(" ".join(sentence_list[(idx + term_len):]), sentiment_dict))
                    new_match_list , new_mismatch_list = get_sentence_sentiment_list(" ".join(sentence_list[(idx + term_len):]), sentiment_dict)
                    match_list.extend(new_match_list)
                    mismatch_list.extend(new_mismatch_list)
                    return match_list, mismatch_list
    return match_list, mismatch_list


def get_tweet_score(tweet, sentiment_dict):
    scores = 0
    sentence_mismatch_list = []
    if "lang" in tweet and "text" in tweet and tweet["lang"] == "en":
        #print tweet["text"]
        sentence_sentiment_list, sentence_mismatch_list = get_sentence_sentiment_list(tweet["text"], sentiment_dict)
        scores = get_sentence_sentiment_scores(sentence_sentiment_list)
    #print scores, sentence_sentiment_list
    return scores, sentence_mismatch_list


def get_sentence_sentiment_scores(sentence_sentiment_list):
    scores = 0
    for term_score in sentence_sentiment_list:
        for term, score in term_score.items():
            scores += score
    return scores


def get_mismatch_term_scores(mismatch_scores_board):
    mismatch_term_score_dict = {}
    for term, score_list in mismatch_scores_board.items():
        scores = 0
        for score in score_list:
            scores += score
        mismatch_term_score_dict[term] = scores / float(len(score_list))
    return mismatch_term_score_dict

def hw(sent_file, tweet_file):
    #f = open('output_filted.txt','w')
    #tweet_file = open(tweet_file)
    sentiment_dict = load_sent(sent_file)
    mismatch_scores_board = {}
    for line in tweet_file:
        tweet = json.loads(line)
        scores, sentence_mismatch_list = get_tweet_score(tweet, sentiment_dict)
        mismatch_scores_board = update_mismatch_score_board(set(sentence_mismatch_list), scores, mismatch_scores_board)
    #print mismatch_scores_board
    mismatch_term_score = get_mismatch_term_scores(mismatch_scores_board)
    for mismatch_term, score in mismatch_term_score.items():
        line = "%s %s" % (mismatch_term, score)
        if len(line.split()) > 2:
            print line.split()[0],"-", line.split()[1], "-", line.split()[2]
        print line
    #print get_sentence_sentiment_list("can't  no fun screwed screwed up", sentiment_dict)

def update_mismatch_score_board(sentence_mismatch_set, scores, mismatch_scores_board):
    for term in sentence_mismatch_set:
        if term not in mismatch_scores_board:
            mismatch_scores_board[term] = []
        mismatch_scores_board[term].append(scores)
    return mismatch_scores_board

def lines(fp):
    print str(len(fp.readlines()))


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    #load_sent(sent_file)
    hw(sent_file, tweet_file)
    #lines(sent_file)
    #lines(tweet_file)





if __name__ == '__main__':
    main()
