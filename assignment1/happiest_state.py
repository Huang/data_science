# -*- coding: utf-8 -*-

import sys
import json
import time



states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

def load_sent(afinnfile):
    # initialize an empty dictionary
    sentiment_dict = {}

    """
    {
        'have':
            {
                'max_len':4,
                '2':{
                      'have fun':4,
                      ...
                    },
                '4':{
                       'have a nice day':5,
                       '...':..,
                    }
            }
        '': ...
    }
    """

    for line in afinnfile:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"

        first_word = term.split(" ")[0]
        term_len = len(term.split(" "))
        if first_word not in sentiment_dict:
            sentiment_dict[first_word] = {}
            sentiment_dict[first_word]["max_len"] = 0
        if term_len not in sentiment_dict[first_word]:
            sentiment_dict[first_word][term_len] = {}
        sentiment_dict[first_word][term_len][term] = int(score)

        # UPDATE MAX_LEN
        if term_len > sentiment_dict[first_word]["max_len"]:
            sentiment_dict[first_word]["max_len"] = term_len

    return sentiment_dict


def get_sentence_sentiment_list(sentence, sentiment_dict):

    #print len(sentiment_dict)
    sentence_list = sentence.split()
    scores = []
    for idx, word in enumerate(sentence_list):
        #print word
        if word not in sentiment_dict:
            #print "<0>"
            continue
        for term_len in range(sentiment_dict[word]['max_len'], 0, -1):
            if term_len not in sentiment_dict[word]:
                #print "<1>"
                continue
            if ( idx + term_len - 1 ) > len(sentence_list):
                #print "<2>"
                continue

            candidate_word = " ".join(sentence_list[idx: (idx + term_len)])
            #print "c> ", sentence_list[idx: (idx + term_len)]
            #print candidate_word
            #print sentence_list, idx, (idx + term_len - 1)
            for term, score in sentiment_dict[word][term_len].items():
                if candidate_word == term:
                    #print "HIT !"
                    scores.append({term:score})
                    scores.extend(get_sentence_sentiment_list(" ".join(sentence_list[(idx + term_len):]), sentiment_dict))
                    return scores
    return scores


def get_tweet_score(tweet, sentiment_dict):
    scores = 0
    if "lang" in tweet and "text" in tweet and tweet["lang"] == "en":
        #print tweet["text"]
        sentence_sentiment_list = get_sentence_sentiment_list(tweet["text"], sentiment_dict)
        scores = get_sentence_sentiment_scores(sentence_sentiment_list)
    #print scores, sentence_sentiment_list
    return scores

def get_tweet_location(tweet):
    if 'place' in tweet and tweet['place'] != None:
    #'country_code' in tweet['place'] and tweet['place']['country_code'] == "US":
        #print "<1>"
        if 'full_name' in tweet['place']:
            #print "place_fullname: ", tweet['place']['full_name']
            for state_candidate in tweet['place']['full_name'].replace(",", "  ").split():
                if state_candidate in states:
                    #time.sleep(0.5)
                    #print "------1--- ", state_candidate
                    return state_candidate
                
    if 'user' in tweet:
        #print "<2>"
        if 'location' in tweet['user']:
            #print "user_localtion:", tweet['user']['location']
            for state_candidate in tweet['user']['location'].replace(",", "  ").split():
                if state_candidate in states:
                    #time.sleep(0.5)
                    #print "____2____ ", state_candidate
                    return state_candidate
    return None

def get_sentence_sentiment_scores(sentence_sentiment_list):
    scores = 0
    for term_score in sentence_sentiment_list:
        for term, score in term_score.items():
            scores += score
    return scores

def hw(sent_file, tweet_file):
    #f = open('output_filted.txt','w')
    #tweet_file = open(tweet_file)
    sentiment_dict = load_sent(sent_file)
    
    state_scores = {}

    for line in tweet_file:
        tweet = json.loads(line)
        scores = get_tweet_score(tweet, sentiment_dict)
        state = get_tweet_location(tweet)
        if state != None:
            if state not in state_scores:
                state_scores[state] = 0
            state_scores[state] += scores

        if len(state_scores) >= 10:
            break
    #print state_scores
    print sorted(state_scores.iteritems(), key=lambda (k,v): (v,k))[-1][0]
    #print get_sentence_sentiment_list("can't  no fun screwed screwed up", sentiment_dict)


def lines(fp):
    print str(len(fp.readlines()))


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    #load_sent(sent_file)
    hw(sent_file, tweet_file)
    #lines(sent_file)
    #lines(tweet_file)





if __name__ == '__main__':
    main()
